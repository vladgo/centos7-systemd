[![build status](https://gitlab.com/vladgo/centos7-systemd/badges/master/build.svg)](https://gitlab.com/vladgo/centos7-systemd/commits/master)

Project for building centos docker image with integrated systemd running in unprivileged mode. 
Based on anarchocurious/ubuntu-systemd-unprivileged.

### Usage example

Start container
```bash
 docker run -dt -v /sys/fs/cgroup:/sys/fs/cgroup:ro --tmpfs /run --tmpfs /run/lock --security-opt seccomp=unconfined --name mytest1 registry.gitlab.com/vladgo/centos7-systemd
```

Start task in container:
```bash
docker exec -i mytest1 systemctl --no-pager
```

Delete container:
```bash
docker rm -f mytest1
```

### Ansible example:

```yaml
---
- hosts: localhost
  gather_facts: no
  connection: local
  vars:
    container_name: mytest1
  tasks:
    - name: start container
      docker_container:
        name: "{{container_name}}"
        hostname: "{{container_name}}"
        image: registry.gitlab.com/vladgo/centos7-systemd
        state: started
        tty: yes
        keep_volumes: no
        volumes:
          - /sys/fs/cgroup:/sys/fs/cgroup:ro

    - name: add container to inventory
      add_host:
        name: "{{container_name}}"
        ansible_host: "{{container_name}}"
        ansible_user: root
        ansible_connection: docker
        groups: docker
      changed_when: false

- hosts: docker
  tasks:
      - name: show host name
        debug: msg={{ansible_hostname}}
```